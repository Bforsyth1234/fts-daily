import { Router } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component } from '@angular/core';
import { ProfileService } from '../services/profile.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss']
})
export class SignupPage {
  public signupForm = new FormGroup({
    email: new FormControl(),
    username: new FormControl(),
    password: new FormControl()
  });

  constructor(
    private authService: AuthService,
    private profileService: ProfileService,
    private router: Router
  ) {}

  public onSubmit(event: Event) {
    event.preventDefault();
    const credentials = {
      email: this.signupForm.value.email,
      username: this.signupForm.value.username,
      password: this.signupForm.value.password,
    };
    this.signUp(credentials);
  }

  signUp(credentials) {
    this.authService.signUp(credentials).then(data => {
      this.profileService.createProfile(data);
      this.router.navigate(['/profile']);
    });
  }
}
