import { AuthService } from './../services/auth/auth.service';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';

import { SignupPage } from './signup.page';

describe('SignupPage', () => {
  let component: SignupPage;
  let fixture: ComponentFixture<SignupPage>;
  let authService: AuthService;
  let injector: TestBed;

 const mockUser = {
   email: 'test@t.com',
   password: 'test@t.com',
 };
  const mockAuthService = {
    signUp: (credentials) => {
      return Promise.resolve(mockUser);
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupPage ],
      imports: [ReactiveFormsModule],
      providers: [{provide: AuthService, useValue: mockAuthService}],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
    injector = getTestBed();
    authService = injector.get(AuthService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call auth service and signup', () => {
    spyOn(authService, 'signUp').and.callThrough();
    const credentials = {
      email: 'test@t.com',
      password: 'test123456'
    };
    component.signUp(credentials);
    expect(authService.signUp).toHaveBeenCalledWith(credentials);
  });
});
