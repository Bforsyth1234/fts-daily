import { ProfileModel } from './../common/profile-model';
import { ProfileService } from './../services/profile.service';
import { Router } from '@angular/router';
import { Tank } from './../common/tank';
import { CameraService } from './../services/camera.service';
import { Component, OnInit } from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';
import { NavController, Platform } from '@ionic/angular';
import { TankService } from '../services/tank.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';


@Component({
  selector: 'app-camera',
  templateUrl: './camera.page.html',
  styleUrls: ['./camera.page.scss'],
})
export class CameraPage implements OnInit {
  image: SafeResourceUrl;
  tankList: Tank[] = [];
  public showTakePicButton = false;
  public selectedTankId: string;
  private profile: ProfileModel;

  constructor(
    public navCtrl: NavController,
    private platform: Platform,
    private camera: Camera,
    private router: Router,
    private cameraService: CameraService,
    private tankService: TankService,
    private profileService: ProfileService,
  ) {
  }

  ngOnInit() {
    this.getTankList();
    this.getProfile();
    this.showTakePicButton = this.platform.is('cordova') ? true : false;
  }

  private getProfile() {
    this.profileService.getProfile().subscribe(data => {
      this.profile = data;
    });
  }

  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options).then((imageData) => {
     const base64Image = 'data:image/jpeg;base64,' + imageData;
     this.image = base64Image;
    }, (err) => {
     // Handle error
    });
  }

  takePictureBrowser() {
    console.log('web');
  }

  getTankList() {
    this.tankService.getTanks().subscribe(data => {
    this.tankList = data.tanks;
    });
  }

  fileUploaded(url) {
    this.image = url;
  }

  savePictureClicked() {
    const imageReadyToBeSaved = this.image && this.selectedTankId;

    imageReadyToBeSaved ? this.savePicture() : this.throwPictureFormError();
    }

  savePicture () {
    this.cameraService.savePicture(this.image, this.selectedTankId, true, this.profile).then(() => {
      this.router.navigate(['/home']);
    });
  }

  throwPictureFormError() {
    console.log('error');
  }
}
