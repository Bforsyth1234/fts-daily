import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CameraPage } from './camera.page';
import { Camera } from '@ionic-native/camera/ngx';
import {WebcamModule} from 'ngx-webcam';
import { ComponentsModule } from '../components/components.module';



const routes: Routes = [
  {
    path: '',
    component: CameraPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WebcamModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  providers: [Camera],
  declarations: [CameraPage]
})
export class CameraPageModule {}
