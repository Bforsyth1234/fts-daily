import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CameraService } from './../services/camera.service';
import { NavController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { TankService } from '../services/tank.service';
import { Camera } from '@ionic-native/camera/ngx';
import { CameraPage } from './camera.page';

fdescribe('CameraPage', () => {
    let comp: CameraPage;
    let fixture: ComponentFixture<CameraPage>;

    beforeEach(() => {
        const cameraServiceStub = {
            savePicture: () => ({})
        };
        const navControllerStub = {};
        const platformStub = {
            is: () => ({})
        };
        const tankServiceStub = {
            getTanks: () => ({
                subscribe: () => ({})
            })
        };
        const cameraStub = {
            DestinationType: {
                FILE_URI: {}
            },
            EncodingType: {
                JPEG: {}
            },
            MediaType: {
                PICTURE: {}
            },
            getPicture: () => ({
                then: () => ({})
            })
        };
        TestBed.configureTestingModule({
            declarations: [ CameraPage ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [
                { provide: CameraService, useValue: cameraServiceStub },
                { provide: NavController, useValue: navControllerStub },
                { provide: Platform, useValue: platformStub },
                { provide: TankService, useValue: tankServiceStub },
                { provide: Camera, useValue: cameraStub }
            ]
        });
        fixture = TestBed.createComponent(CameraPage);
        comp = fixture.componentInstance;
    });

    it('can load instance', () => {
        expect(comp).toBeTruthy();
    });

    it('tankList defaults to: []', () => {
        expect(comp.tankList).toEqual([]);
    });

    it('showTakePicButton defaults to: false', () => {
        expect(comp.showTakePicButton).toEqual(false);
    });

    describe('ngOnInit', () => {
        it('makes expected calls', () => {
            const platformStub: Platform = fixture.debugElement.injector.get(Platform);
            spyOn(comp, 'getTankList');
            spyOn(platformStub, 'is');
            comp.ngOnInit();
            expect(comp.getTankList).toHaveBeenCalled();
            expect(platformStub.is).toHaveBeenCalled();
        });
    });

    describe('takePicture', () => {
        it('makes expected calls', () => {
            const cameraStub: Camera = fixture.debugElement.injector.get(Camera);
            spyOn(cameraStub, 'getPicture');
            comp.takePicture();
            expect(cameraStub.getPicture).toHaveBeenCalled();
        });
    });

    describe('getTankList', () => {
        it('makes expected calls', () => {
            const tankServiceStub: TankService = fixture.debugElement.injector.get(TankService);
            spyOn(tankServiceStub, 'getTanks');
            comp.getTankList();
            expect(tankServiceStub.getTanks).toHaveBeenCalled();
        });
    });

    describe('savePictureClicked', () => {
        it('makes expected calls', () => {
            spyOn(comp, 'savePicture');
            spyOn(comp, 'throwPictureFormError');
            comp.savePictureClicked();
            expect(comp.savePicture).toHaveBeenCalled();
            expect(comp.throwPictureFormError).toHaveBeenCalled();
        });
    });

    describe('savePicture', () => {
        it('makes expected calls', () => {
            const cameraServiceStub: CameraService = fixture.debugElement.injector.get(CameraService);
            spyOn(cameraServiceStub, 'savePicture');
            comp.savePicture();
            expect(cameraServiceStub.savePicture).toHaveBeenCalled();
        });
    });

});
