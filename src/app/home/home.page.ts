import { CameraService } from './../services/camera.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public allPics;
  constructor(
    public cameraService: CameraService,
  ) {
    this.allPics = this.cameraService.getAllPictures();
  }
}
