import { AuthService } from './../../services/auth/auth.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AngularFireStorage } from 'angularfire2/storage';
import { finalize } from 'rxjs/operators';
import { UUID } from 'angular2-uuid';


@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {
  @Output() fileUrl = new EventEmitter();
  public uuid: string;
  public newUuid: string;
  constructor(
    private storage: AngularFireStorage,
    public authService: AuthService
  ) {
    this.uuid = this.authService.getUserId();
    const URL = 'path_to_api';
  }

  ngOnInit() {
    this.newUuid = UUID.UUID();
  }

  uploadFile(event) {
    const file = event.target.files[0];
    const filePath = `${this.uuid}/${this.newUuid}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    // observe percentage changes
    // this.uploadPercent = task.percentageChanges();
    // get notified when the download URL is available
    task.snapshotChanges().pipe(
      finalize(() => fileRef.getDownloadURL().subscribe(data => {
        this.fileUrl.emit(data);
      }))
    )
      .subscribe();
  }

}
