import { Observable } from 'rxjs';
import { AuthService } from './auth/auth.service';
import { ProfileModel } from './../common/profile-model';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';


@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private profileDoc: AngularFirestoreDocument<any>;
  profile: ProfileModel;

  constructor(private angularFireStore: AngularFirestore, private authService: AuthService) {
    if (this.authService.isUserLoggedIn()) { this.setProfileDoc(); }
  }

  public setProfileDoc() {
    this.profileDoc = this.angularFireStore.doc(`profiles/${this.getUserId()}/`);
  }

  public getUserId() {
    return this.authService.getUserId();
  }

  public getProfile(): Observable<any> {
    return this.profileDoc.valueChanges();
  }

  public updateProfile(profile: ProfileModel) {
    this.profileDoc.update(profile);
  }

  public createProfile(userCred: firebase.auth.UserCredential) {
    this.profileDoc = this.angularFireStore.doc(`profiles/${userCred.user.uid}`);
    this.profileDoc.set(this.createNewProfile(userCred));
  }

  public createNewProfile(userCred: firebase.auth.UserCredential) {
    return {
      email: userCred.user.email,
      uid: userCred.user.uid,
    };
  }

  public getProfileDocUrl() {
    return this.profileDoc;
  }
}
