import { PictureDataModel } from './../common/picture-data-model';
import { AuthService } from './auth/auth.service';
import { Injectable } from '@angular/core';
import { AngularFirestoreDocument, AngularFirestore } from 'angularfire2/firestore';
import { ProfileModel } from '../common/profile-model';


@Injectable({
  providedIn: 'root'
})
export class CameraService {
  private picturesDoc: AngularFirestoreDocument<any>;
  private pictures: PictureDataModel[] = [];

  constructor(
    private authService: AuthService,
    private angularFirestore: AngularFirestore,
  ) {
    this.picturesDoc = this.angularFirestore.doc(`pictures/allpics/`);
    this.getPictures();
  }
  public getUserId() {
    return this.authService.getUserId();
  }

  public getPictures() {
    return this.picturesDoc.valueChanges().subscribe(data => {
      if (data) {
        this.pictures = data.pictures;
      }
    });
  }

  public getAllPictures() {
    return this.pictures;
  }

  public savePicture(url, tankId: string, fts: boolean, profile: ProfileModel) {
    const pictureData: PictureDataModel = this.generatePictureData(url, tankId, fts, profile);
    this.pictures.push(pictureData);
    const pictureObj = {
      pictures: this.pictures
    };
    return this.picturesDoc.update(pictureObj);
  }

  private generatePictureData(url: string, tankId: string, fts: boolean, profile: ProfileModel) {
    const newPictureData: PictureDataModel = {
      url: url,
      tank: tankId,
      date: new Date(),
      fts: fts,
      profile: profile
    };
    return newPictureData;
  }
}
