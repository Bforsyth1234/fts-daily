import { Tank } from './../common/tank';
import { AuthService } from './auth/auth.service';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TankService {
  private userId: string;
  private tankDoc: AngularFirestoreDocument<any>;

  constructor(
    private authService: AuthService,
    private angularFireStore: AngularFirestore,
  ) {
    this.userId = this.authService.getUserId();
  }

  public createNewTankList(tanks: Tank[]) {
    this.tankDoc = this.angularFireStore.doc(`userstanks/${this.userId}`);
    this.tankDoc.set({ tanks });
  }

  public updateTank(tanks: Tank[]) {
    this.tankDoc = this.angularFireStore.doc(`userstanks/${this.userId}`);
    this.tankDoc.update({tanks});
  }

  public getTanks() {
    this.tankDoc = this.angularFireStore.doc(`userstanks/${this.userId}`);
    return this.tankDoc.valueChanges();
  }
}
