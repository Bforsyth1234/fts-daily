import { Credentials } from './../../models/credentials.model';
import { Observable, defer } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';
import { TestBed, inject } from '@angular/core/testing';

import { AuthService } from './auth.service';

const mockAngularFireAuth = {
  signInWithEmailAndPassword: (credentials) => {
    return Promise.resolve({email: 'test@t.com',
    password: 'test@t.com'});
  },
  createUserWithEmailAndPassword: (credentials) => {
    return Promise.resolve({email: 'test@t.com',
    password: 'test@t.com'});
  },
  authState: () => {
    return Observable.throw({});
  }
};

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService, {provide: AngularFireAuth, useValue: mockAngularFireAuth}]
    });
  });

  it('should be created', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));



  xit('should return an observable on sign in', inject(
    [AuthService],
    (service: AuthService) => {
    const credentials = {email: 'test@t.com',
    password: 'test@t.com'};
    let result;
    service.signInWithEmail(credentials).then(data => {
      result = data;
    });
    expect(result.email).toEqual(credentials);
  }));
});
