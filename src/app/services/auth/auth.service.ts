import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user: firebase.User;
  public isLoggedIn: boolean;

  constructor(public afAuth: AngularFireAuth, public router: Router) {
    this.getUser();
  }

  getUser() {
    this.afAuth.authState.subscribe(data => {
      if (data) {
        this.user = data;
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    });
  }

  public getUserId() {
    return this.user.uid;
  }

  isUserLoggedIn() {
    return this.isLoggedIn;
  }

  public signInWithEmail(credentials) {
    return this.afAuth.auth.signInWithEmailAndPassword(
      credentials.email,
      credentials.password
    );
  }

  public signUp(credentials) {
    return this.afAuth.auth.createUserWithEmailAndPassword(
      credentials.email,
      credentials.password
    );
  }

  public logOut() {
    return this.afAuth.auth.signOut();
  }
}
