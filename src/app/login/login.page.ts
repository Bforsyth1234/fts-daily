import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../services/auth/auth.service';
import { Credentials } from './../models/credentials.model';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  public loginForm = new FormGroup({
    email: new FormControl(),
    password: new FormControl()
  });

  constructor(public authService: AuthService, public router: Router) { }

  public onSubmit(event: Event) {
    const credentials: Credentials = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    };
    this.login(credentials);
  }

  public login(credentials: Credentials) {
    this.authService.signInWithEmail(credentials).then(data => {
      console.log('data = ');
      console.log(data);
      this.router.navigate(['/home']);
    }).catch(error => {
      console.log('error = ');
      console.log(error);
    });
  }
}
