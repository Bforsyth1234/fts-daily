import { Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { Credentials } from './../models/credentials.model';
import { AuthService } from '../services/auth/auth.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';

import { LoginPage } from './login.page';

describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;
  let authService: AuthService;
  let injector: TestBed;

  const mockRouter = {
    navigate: () => {
      return Promise.resolve({});
    }
  };
  const mockUser: Credentials = {
    email: 'test@t.com',
    password: 'test@t.com',
  };
   const mockAuthService = {
    signInWithEmail: (credentials) => {
       return Promise.resolve(mockUser);
     }
   };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPage ],
      imports: [ReactiveFormsModule],
      providers: [{provide: Router, useValue: mockRouter}, {provide: AuthService, useValue: mockAuthService}],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
    injector = getTestBed();
    authService = injector.get(AuthService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call auth service to login user', () => {
    spyOn(authService, 'signInWithEmail').and.callThrough();
    // spyOn(router, 'navigate').and.callThrough();
    const credentials = {
      email: 'test@t.com',
      password: 'test123456'
    };
    component.login(credentials);
    expect(authService.signInWithEmail).toHaveBeenCalledWith(credentials);
  });

});
