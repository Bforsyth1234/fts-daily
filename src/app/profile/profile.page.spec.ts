import { ProfileService } from './../services/profile.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { Router } from '@angular/router';
import { AuthService } from './../services/auth/auth.service';
import { ReactiveFormsModule, FormGroup, FormControl } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilePage } from './profile.page';
import { BehaviorSubject, Observable, of } from 'rxjs';

xdescribe('ProfilePage', () => {
  let component: ProfilePage;
  let fixture: ComponentFixture<ProfilePage>;

  const mockAuthService = {
    logOut: () => {
       return Promise.resolve('mockUser');
     },
     getUserId: () => {
       return null;
     }
   };
   const FirestoreStub = {
    collection: (name: string) => ({
      doc: (_id: string) => ({
        valueChanges: () => new BehaviorSubject({ foo: 'bar' }),
        set: (_d: any) => new Promise((resolve, _reject) => resolve()),
      }),
    }),
  };
  const ProfileServiceStub = {
    updateProfile: () =>  of({'test': null}),
    getProfile: () => {
     return of({'data': {
              'email': 'testemail@test.com',
              'userName': 'testuserName',
              'profilePicUrl': 'profilePicUrl'
            }
          });
  }
  };

  beforeEach(async(() => {
    const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
    TestBed.configureTestingModule({
      declarations: [ ProfilePage ],
      imports: [ReactiveFormsModule],
      providers: [
        {provide: AngularFirestore, useValue: FirestoreStub},
        {provide: ProfileService, useValue: ProfileServiceStub},
        {provide: Router, useValue: routerSpy},
        {provide: AuthService, useValue: mockAuthService}],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set profileForm email to testemail', () => {
    component.profileForm = new FormGroup({
      email: new FormControl(),
      userName: new FormControl(),
      profilePicUrl: new FormControl(),
    });
    spyOn(component, 'getCurrentProfile').and.callThrough();

    component.getCurrentProfile();
    // expect(component.getCurrentProfile).toHaveBeenCalled();
    // expect(component.profileForm.get('email').value).toEqual('testemail@test.com');
  });

});
