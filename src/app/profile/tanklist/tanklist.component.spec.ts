import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TanklistComponent } from './tanklist.component';

describe('TanklistComponent', () => {
  let component: TanklistComponent;
  let fixture: ComponentFixture<TanklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TanklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TanklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
