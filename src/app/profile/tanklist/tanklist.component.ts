import { TankService } from './../../services/tank.service';
import { Tank } from './../../common/tank';
import { Component, OnInit } from '@angular/core';
import { UUID } from 'angular2-uuid';


@Component({
  selector: 'app-tanklist',
  templateUrl: './tanklist.component.html',
  styleUrls: ['./tanklist.component.scss']
})
export class TanklistComponent implements OnInit {
  public tanks: Array<Tank> = [];

  constructor(
    public tankService: TankService,
  ) { }

  ngOnInit() {
    this.getTanks();
  }

  private getTanks() {
    this.tankService.getTanks().subscribe(data => {
      if (!data) {
        this.generateNewBlankTank();
      }
      // Firebase sends back and object when there is only 1 item.
      // This converts it from object into an array so ngFor can itereate through it.
      !data.tanks.length ? this.tanks.push(data.tanks) : this.tanks = data.tanks;
    });
  }

  generateNewBlankTank(): Tank {
   return {
      name: '',
      age: 0,
      size: 0,
      type: '',
      uuid: UUID.UUID(),
      };
  }

  private createTankList(tanks: Tank[]) {
    this.tanks = tanks;
    this.tankService.createNewTankList(this.tanks);
  }

  public updateTank(upDatedTank: Tank) {
    for (let i = 0; i < this.tanks.length; i++) {
      const uuidMatch = this.tanks[i].uuid === upDatedTank.uuid;
      if (this.tanks[i] && uuidMatch)  {
        this.tanks[i] = upDatedTank;
      }
    }
    this.tankService.updateTank(this.tanks);
  }

  public addNewBlankTank() {
    const newTank: Tank = this.generateNewBlankTank();
    this.tanks.push(newTank);
    this.tankService.updateTank(this.tanks);
  }

  public deleteTank(uuid: string) {
    this.tanks = this.tanks.filter(tank => tank.uuid !== uuid);
    this.tankService.updateTank(this.tanks);
  }
}
