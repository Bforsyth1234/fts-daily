import { TankService } from '../../services/tank.service';
import { Tank } from './../../common/tank';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-tank',
  templateUrl: './tank.component.html',
  styleUrls: ['./tank.component.scss']
})
export class TankComponent implements OnInit {
  @Input() tank: Tank;
  @Output() updateTank: EventEmitter<Tank> = new EventEmitter();
  @Output() deleteTank: EventEmitter<string> = new EventEmitter();

  public tankForm = new FormGroup({
    name: new FormControl(),
    size: new FormControl(),
    age: new FormControl(),
    type: new FormControl(),
    uuid: new FormControl(),
  });

  constructor(public tankService: TankService) {
  }

  ngOnInit() {
    if (this.tank) {
      this.tankForm.setValue({
        name: this.tank.name,
        size: this.tank.size,
        age: this.tank.age,
        type: this.tank.type,
        uuid: this.tank.uuid,
      });
    }
  }

  onSubmit() {
    this.tank = this.tankForm.value;
    this.updateTank.emit(this.tankForm.value);
  }

  emitDeleteTank() {
    this.deleteTank.emit(this.tank.uuid);
  }
}
