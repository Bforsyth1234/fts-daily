import { ProfileService } from './../services/profile.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import { Component } from '@angular/core';
import { ProfileModel } from '../common/profile-model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage {
  private formBuilder: FormBuilder;
  public tankForm = new FormGroup({});
  public profileForm = new FormGroup({
    email: new FormControl(),
    userName: new FormControl(),
    profilePicUrl: new FormControl(),
  });

  constructor(
    public authService: AuthService,
    public router: Router,
    public profileService: ProfileService,
  ) {
    this.getCurrentProfile();
  }

  public getCurrentProfile() {
    this.profileService.getProfile().subscribe(data => {
      this.profileForm.setValue({
        email: data.email,
        userName: '',
        profilePicUrl: '',
      });
    });
  }

  public logOut() {
    this.authService.logOut().then(() => {
      this.router.navigate(['/login']);
    });
  }

  public onSubmit() {
    const newProfile: ProfileModel = {
      email: 'test@test.com',
      password: 'test',
      userName: 'string',
      profilePicUrl: 'string',
    };
    this.profileService.updateProfile(newProfile);
  }

  public addTank() {
    console.log('clicked');
  }

  public newTank(): FormGroup {
    return new FormGroup ({
      name: new FormControl(),
      size: new FormControl(),
      type: new FormControl(),
    });
  }

}
