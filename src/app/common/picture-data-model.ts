import { ProfileModel } from './profile-model';
export class PictureDataModel {
    url: string;
    tank: string;
    date: Date;
    fts: boolean;
    profile: ProfileModel;

    constructor(url, tank, date, fts, profile) {
        this.url = url;
        this.tank = tank;
        this.date = date;
        this.fts = fts;
        this.profile = profile;
    }
 }
