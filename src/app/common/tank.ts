export class Tank {
    name: string;
    size: number;
    age: number;
    type: string;
    uuid: string;

    constructor(name, size, age, type, uuid) {
        this.name = name;
        this.size = size;
        this.age = age;
        this.type = type;
        this.uuid = uuid;
    }
}
