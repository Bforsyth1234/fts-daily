export class ProfileModel {
    email: string;
    password: string;
    userName: string;
    profilePicUrl: string;
}
